
// global variable & function

let pbatu = document.getElementById("playerbatu");
let pgunting = document.getElementById("playergunting");
let pkertas = document.getElementById("playerkertas");
let cbatu = document.getElementById("cpubatu");
let cgunting = document.getElementById("cpugunting");
let ckertas = document.getElementById("cpukertas");
let middle = document.getElementById("middletext");

function draw() {
    middle.innerHTML = "Draw";
    middle.style.backgroundColor = "green";
    middle.style.color = "white";
    middle.style.width = 150;
    middle.style.transform = "rotate(-20deg)"
    console.log("game seri");
}
function player1win() {
    middle.innerHTML = "PLAYER 1 WIN";
    middle.style.backgroundColor = "lightgreen";
    middle.style.color = "white";
    middle.style.width = 150;
    middle.style.transform = "rotate(-20deg)"
    console.log("player 1 menang");
}
function comwin() {
    middle.innerHTML = "COM WIN";
    middle.style.backgroundColor = "lightgreen";
    middle.style.color = "white";
    middle.style.width = 150;
    middle.style.transform = "rotate(-20deg)"
    console.log("com menang");
}
function batustyle() {
    pbatu.style.backgroundColor = "lightgrey";
    pbatu.style.borderRadius = "20px";
    pbatu.style.display = 'block';
    pbatu.onclick = null;
}
function guntingstyle() {
    pgunting.style.backgroundColor = "lightgrey";
    pgunting.style.borderRadius = "20px";
    pgunting.style.display = 'block';
    pgunting.onclick = null;
}
function kertasstyle() {
    pkertas.style.backgroundColor = "lightgrey";
    pkertas.style.borderRadius = "20px";
    pkertas.style.display = 'block';
    pkertas.onclick = null;
}

let array = [0, 1, 2];

let arrayrandom = Math.floor(Math.random() * array.length);


// class 0 (player batu) 

class playerbatu {

    run() {

        console.log("pilihan user batu");

        if (pbatu.clicked = true && arrayrandom === 0) {
            cbatu.style.backgroundColor = "lightgrey";
            cbatu.style.borderRadius = "20px";
            batustyle();
            pgunting.style.display = 'block';
            pgunting.onclick = null;
            pkertas.style.display = 'block';
            pkertas.onclick = null;
            draw();

        } else if (pbatu.clicked = true && arrayrandom === 1) {
            cgunting.style.backgroundColor = "lightgrey";
            cgunting.style.borderRadius = "20px";
            batustyle();
            pgunting.style.display = 'block';
            pgunting.onclick = null;
            pkertas.style.display = 'block';
            pkertas.onclick = null;
            player1win();

        } else if (pbatu.clicked = true && arrayrandom === 2) {
            ckertas.style.backgroundColor = "lightgrey";
            ckertas.style.borderRadius = "20px";
            batustyle();
            pgunting.style.display = 'block';
            pgunting.onclick = null;
            pkertas.style.display = 'block';
            pkertas.onclick = null;
            comwin();

        }
    }
}

// class 1 (player gunting) 

class playergunting {

    run() {

        console.log("pilihan user gunting");

        if (pgunting.clicked = true && arrayrandom === 0) {
            cbatu.style.backgroundColor = "lightgrey";
            cbatu.style.borderRadius = "20px";
            guntingstyle();
            pbatu.style.display = 'block';
            pbatu.onclick = null;
            pkertas.style.display = 'block';
            pkertas.onclick = null;
            comwin();

        } else if (pgunting.clicked = true && arrayrandom === 1) {
            cgunting.style.backgroundColor = "lightgrey";
            cgunting.style.borderRadius = "20px";
            guntingstyle();
            pbatu.style.display = 'block';
            pbatu.onclick = null;
            pkertas.style.display = 'block';
            pkertas.onclick = null;
            draw();

        } else if (pgunting.clicked = true && arrayrandom === 2) {
            ckertas.style.backgroundColor = "lightgrey";
            ckertas.style.borderRadius = "20px";
            guntingstyle();
            pbatu.style.display = 'block';
            pbatu.onclick = null;
            pkertas.style.display = 'block';
            pkertas.onclick = null;
            player1win();

        }
    }
}

// class 2 (player kertas) 

class playerkertas {

    run() {

        console.log("pilihan user kertas");

        if (pkertas.clicked = true && arrayrandom === 0) {
            cbatu.style.backgroundColor = "lightgrey";
            cbatu.style.borderRadius = "20px";
            kertasstyle();
            pgunting.style.display = 'block';
            pgunting.onclick = null;
            pbatu.style.display = 'block';
            pbatu.onclick = null;
            player1win();

        } else if (pkertas.clicked = true && arrayrandom === 1) {
            cgunting.style.backgroundColor = "lightgrey";
            cgunting.style.borderRadius = "20px";
            kertasstyle();
            pgunting.style.display = 'block';
            pgunting.onclick = null;
            pbatu.style.display = 'block';
            pbatu.onclick = null;
            comwin();

        } else if (pkertas.clicked = true && arrayrandom === 2) {
            ckertas.style.backgroundColor = "lightgrey";
            ckertas.style.borderRadius = "20px";
            kertasstyle();
            pgunting.style.display = 'block';
            pgunting.onclick = null;
            pbatu.style.display = 'block';
            pbatu.onclick = null;
            draw();

        }
    }
}

// fungsi onclick

function cpu0() {
    var x = new playerbatu();
    x.run();
}
function cpu1() {
    var x = new playergunting();
    x.run();
}
function cpu2() {
    var x = new playerkertas();
    x.run();
}
